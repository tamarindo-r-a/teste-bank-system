package br.com.banksystem.teste.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.banksystem.teste.exception.BankSystemException;
import br.com.banksystem.teste.infra.ServiceFinder;
import br.com.banksystem.teste.model.ClockAngle;
import br.com.banksystem.teste.service.ClockService;

@Path("clock")
@Produces("application/json")
@Consumes("application/json")
public class ClockRest
{

	@GET
	@Path("/{hours:\\d+}/{minutes:(\\d+)?}")
	public ClockAngle getClockAngle(@PathParam("hours") Integer hours, @PathParam("minutes") Integer minutes) throws BankSystemException
	{
		ClockService clockService = ServiceFinder.getInstance().lookup(ClockService.class, "teste-bank-system");
		return clockService.getClockAngle(hours, minutes);
	}
	
	@GET
	@Path("/{hours:\\d+}")
	public ClockAngle getClockAngle(@PathParam("hours") Integer hours) throws BankSystemException
	{
		ClockService clockService = ServiceFinder.getInstance().lookup(ClockService.class, "teste-bank-system");
		return clockService.getClockAngle(hours, null);
	}
	
}
