package br.com.banksystem.teste.exception;

@SuppressWarnings("serial")
public class BankSystemRuntimeException extends RuntimeException
{

	public BankSystemRuntimeException()
	{
		super();
	}
	
	public BankSystemRuntimeException(String msg, Exception e)
	{
		super(msg, e);
	}
	
}
