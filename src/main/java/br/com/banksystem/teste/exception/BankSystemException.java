package br.com.banksystem.teste.exception;

@SuppressWarnings("serial")
public class BankSystemException extends Exception
{
	
	private Integer code;

	public BankSystemException()
	{
		super();
	}
	
	public BankSystemException(String msg, Integer code)
	{
		super(msg);
		this.code = code;
	}
	
	public BankSystemException(String msg, Integer code, Exception e)
	{
		super(msg, e);
		this.code = code;
	}
	
	public Integer getCode()
	{
		return code;
	}
	
}
