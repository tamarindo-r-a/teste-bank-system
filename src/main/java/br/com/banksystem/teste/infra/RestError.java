package br.com.banksystem.teste.infra;

public class RestError
{

	private String message;

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
	
}
