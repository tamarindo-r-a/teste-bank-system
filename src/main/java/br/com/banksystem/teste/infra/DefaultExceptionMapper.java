package br.com.banksystem.teste.infra;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception>
{

	@Override
	public Response toResponse(Exception e)
	{
		RestError error = new RestError();
		error.setMessage("Erro interno do servidor. Por favor, tente novamente mais tarde.");
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
	}

}
