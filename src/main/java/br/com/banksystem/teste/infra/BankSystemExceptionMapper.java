package br.com.banksystem.teste.infra;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.banksystem.teste.exception.BankSystemException;

@Provider
public class BankSystemExceptionMapper implements ExceptionMapper<BankSystemException>
{

	@Override
	public Response toResponse(BankSystemException e)
	{
		RestError error = new RestError();
		error.setMessage(e.getMessage());
		return Response.status(e.getCode()).entity(error).build();
	}

}
