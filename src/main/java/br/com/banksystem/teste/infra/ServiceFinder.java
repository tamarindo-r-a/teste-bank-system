package br.com.banksystem.teste.infra;

import javax.naming.InitialContext;

import br.com.banksystem.teste.exception.BankSystemRuntimeException;

public class ServiceFinder
{
	
	private static ServiceFinder instance = new ServiceFinder();
	
	@SuppressWarnings("unchecked")
    public <T> T lookup(Class<T> serviceClass, String moduleName) throws BankSystemRuntimeException
	{
		String jndi = getLocalServiceJndiAddress(serviceClass, moduleName);
		
		try
		{
			InitialContext context = new InitialContext();
			Object service = context.lookup(jndi);
			return (T) service;
		}
		catch (Exception e)
		{
			throw new BankSystemRuntimeException("Serviço " + serviceClass + " não encontrado: " + jndi , e);
		}
	}
	
	private String getLocalServiceJndiAddress(Class<?> serviceClass, String moduleName)
	{
		String shortName = serviceClass.getSimpleName();
		
		String fullInterfaceName = serviceClass.getCanonicalName();
		
		return "java:global/" + moduleName + "/" + shortName + "!" + fullInterfaceName;		
	}
	
	public static ServiceFinder getInstance()
	{
		return instance;
	}
}