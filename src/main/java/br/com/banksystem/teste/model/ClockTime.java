package br.com.banksystem.teste.model;

public class ClockTime
{

	private Integer hours;
	private Integer minutes;
	
	public Integer getHours()
	{
		return hours;
	}
	
	public void setHours(Integer hours)
	{
		this.hours = hours;
	}
	
	public Integer getMinutes()
	{
		return minutes;
	}
	
	public void setMinutes(Integer minutes)
	{
		this.minutes = minutes == null ? Integer.valueOf(0) : minutes;
	}
	
	@Override
    public int hashCode()
	{
		return hours * 60 + minutes;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(obj instanceof ClockTime)
		{
			ClockTime other = (ClockTime) obj;
			
			if(hours == null)
			{
				if(other.getHours() != null)
					return false;
			}
			else if(!hours.equals(other.getHours()))
				return false;
			
			if(minutes == null)
			{
				if(other.getMinutes() != null)
					return false;
			}
			else if(!minutes.equals(other.getMinutes()))
				return false;
			
			return true;
		}
		
		return false;
	}
	
}
