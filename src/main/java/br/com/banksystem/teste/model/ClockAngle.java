package br.com.banksystem.teste.model;

public class ClockAngle
{

	Double angle;

	public Double getAngle()
	{
		return angle;
	}

	public void setAngle(Double angle)
	{
		this.angle = angle;
	}
	
}
