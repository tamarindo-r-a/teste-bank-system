package br.com.banksystem.teste.service;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Singleton;

import br.com.banksystem.teste.exception.BankSystemException;
import br.com.banksystem.teste.model.ClockAngle;
import br.com.banksystem.teste.model.ClockTime;

@Singleton
public class ClockService
{
	
	private Map<ClockTime, ClockAngle> clockAngles = new HashMap<ClockTime, ClockAngle>();
	
	public ClockAngle getClockAngle(Integer hours, Integer minutes) throws BankSystemException
	{
		ClockTime time = new ClockTime();
		ClockAngle angle = null;
		
		time.setHours(hours);
		time.setMinutes(minutes);
		
		if(isClockTimeValid(time))
		{
			if(clockAngles.containsKey(time))
			{
				angle = clockAngles.get(time);
			}
			else
			{
				angle = computeClockAngle(time);
				clockAngles.put(time, angle);
			}
		}
		else
		{
			throw new BankSystemException("O horário dado não é válido.", 400);
		}
		
		return angle;
	}
	
	private static boolean isClockTimeValid(ClockTime time)
	{
		Integer hours = time.getHours();
		Integer minutes = time.getMinutes();
		
		return hours == null ? false : hours < 24 && hours > -1 && minutes < 60 && minutes > -1;
	}
	
	private static ClockAngle computeClockAngle(ClockTime time)
	{
		ClockAngle angle = new ClockAngle();
		Integer hours = time.getHours() > 12 ? time.getHours() - 12 : time.getHours();
		Integer minutes = time.getMinutes();
		Double hoursAngle = 30.0 * hours + 0.5 * minutes; // (hours * 360.0 / 12.0) + (minutes * 30.0 / 60.0);
		Double minutesAngle = 6.0 * minutes; // minutes * 360.0 / 60.0;
		
		angle.setAngle(Math.abs(hoursAngle - minutesAngle));
		
		if(angle.getAngle() > 180.0)
		{
			angle.setAngle(360.0 - angle.getAngle());
		}
		
		return angle;
		
	}
	
}
