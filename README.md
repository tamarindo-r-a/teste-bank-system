# Teste BankSystem

Teste de aplicação REST com serviço EJB.

## Instruções

1. Copie o Dockerfile
1. Crie a imagem: `docker build -t raitamarindo/teste-banksystem .`
1. Execute a aplicação: `docker run -p 8080:8080 raitamarindo/teste-banksystem`
1. Acesse http://_seu-host_:8080/teste-bank-system/_hora_/_minuto(opcional)_
	1. Exemplos: (O servidor 159.89.129.124 ficará disponível temporariamente)
		* GET [159.89.129.124:8080/teste-bank-system/rest/clock/6](http://159.89.129.124:8080/teste-bank-system/rest/clock/6)
		* GET [159.89.129.124:8080/teste-bank-system/rest/clock/3/45](http://159.89.129.124:8080/teste-bank-system/rest/clock/3/45)
		* GET [159.89.129.124:8080/teste-bank-system/rest/clock/21/02](http://159.89.129.124:8080/teste-bank-system/rest/clock/21/02)
		* GET [159.89.129.124:8080/teste-bank-system/rest/clock/12](http://159.89.129.124:8080/teste-bank-system/rest/clock/12)
		* GET [159.89.129.124:8080/teste-bank-system/rest/clock/0/30](http://159.89.129.124:8080/teste-bank-system/rest/clock/0/30)

**Recomendo uma máquina de no mínimo 2 GB de memória.**
